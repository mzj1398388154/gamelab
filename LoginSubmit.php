<!doctype html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <title>Log In Result</title>
  </head>

<body class="text-center">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="index.php">GameLab</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="news.php">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="updateGame.php">Update Database</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="view.php">FPS Games</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="signup.php">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Log In</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <div  style="margin-top:100px;">
    <?php

    $email=$_REQUEST['email'];
    $email = "'".$email."'";
    $password=$_REQUEST['password'];
    $boolean="false";

    $cluster = Cassandra::cluster()
        ->withContactPoints('172.23.99.89','172.23.99.35')
        ->withPort(9042)
        ->build();
    $keyspace = 'cloudcomputing';
    $session = $cluster->connect($keyspace);
    $statement = new Cassandra\SimpleStatement(
        "SELECT * FROM user WHERE email = $email"
    );
    $future = $session->executeAsync($statement);
    $result = $future->get();
    foreach($result as $row){
        if($row['password'] == $password){
            $boolean = "true";
            $name = $row['first_name'];
        }
    }




    if($boolean == "true"){
        echo "log in success";
        echo "<br/>";
        echo "hi, ".$name;
        echo '<a class="btn btn-primary" href="index.php" role="button">back to home page</a>';
    }else{
        echo "log in fail, try again";
        echo "<br/>";
        echo '<a class="btn btn-primary" href="login.php" role="button">Log In</a>';
    }
    ?>
    </div>
</body>
	
</html>
