<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="3208">
    <meta name="author" content="Lianyu Zhao">
    <title>Game Lab</title>
    <link href= "bootstrap.min.css" rel="stylesheet">
    <link href="index.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap.min.js"></script>
  </head>

  <body>

    <div class="container" style="width:10px">

      <nav class="navbar-default navbar-fixed-top" style="height:56px; background-color: black">
        <div class="container" style="padding:3px" >
          <div class="navbar-header">
            <a class="navbar-brand" style="color:white">GameLab</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="nav-item">
                    <a class="nav-link" href="news.php">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="updateGame.php">Update Database</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="view.php">FPS Games</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="signup.php">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Log In</a>
                </li>
            </ul>
            <form class="nav navbar-nav navbar-right form-inline my-2 my-lg-0" style="padding:7px">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
          </div>
          
        </div>
        
      </nav>
    
    </div>

    <div class="container">
      <div class="page-header">
        <h1>Game Gallery</h1>
        <p class="lead">Where you find games.</p>
      </div>

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img class="first-slide" src="roe4.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Ring of Elysium</h1>
                <p><a class="btn btn-lg btn-primary" href="gameInfo.php" role="button">Game Detail</a></p>
              </div>
            </div>
          </div>
          <div class="item">
            <img class="second-slide" src="csgo-money-hero.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Counter Strike: Global Offensive</h1>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Game Detail</a></p>
              </div>
            </div>
          </div>
          <div class="item">
            <img class="third-slide" src="assassin.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Assassin's Creed2</h1>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Game Detail</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-minus"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-plus"></span></span>
          <span class="sr-only">Next</span>
        </a>
        
      </div>
  
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>3208</p>
      </footer>

    </div>


  </body>
</html>
