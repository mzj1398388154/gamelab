<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<title>Sign Up</title>
</head>

<body class="text-center">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="index.php">GameLab</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="news.php">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="updateGame.php">Update Database</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="view.php">FPS Games</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="signup.php">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Log In</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <form method="POST" action="SignupSubmit.php"  style="width:500px; margin-left:auto; margin-right:auto; margin-top:100px;">
        <label>First Name</label>
        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" required>
        <br/>
        <label>Last Name</label>
        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required>
        <br/>
        <label>Gender</label>
        <input type="radio" name="gender" value="Male" class="form-control" >Male<br/>
        <input type="radio" name="gender" value="Female" class="form-control">Female
        <br/>
        <label>Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required>
        <br/>
        <label>Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <br/>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>		
</body>
</html>